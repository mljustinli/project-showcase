//converts vertical scrolling to horizontal
$(document).ready(function() {
    $('html, body, *').mousewheel(function(e, delta) {
        this.scrollLeft -= (delta * 5);
        e.preventDefault();
    });
});

// scrolls if the scroll button is pressed and hides it if it's pressed
// or the user scrolls a little
$(document).ready(function() {
    $('#scroll-button').click(function() {
        $('#project-view').animate({
            scrollLeft: $("#station-1").position().left
        }, 500);

        $('#scroll-button').fadeOut("slow");
    });

    var leftOfDiv = $('#station-1').position().left;
    $('#project-view').scroll(function() {
        if ($('#project-view').scrollLeft() >= leftOfDiv / 2) {
            $('#scroll-button').fadeOut("slow");
        }
    });
});
