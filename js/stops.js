/*
 * To add a new project, add a new object below this and also add the object
 * to the array.
 *
 * Also the date is 0 indexed.
 */

var foodWasteApp = {
    startDate: new Date(2019, 9),
    title: "Waste!",
    description: `
    Currently developing a leftover and grocery management web application.
    The app will send users reminders about leftovers that they have and also
    remind users to use certain ingredients before they start to cook.
    `,
    tags: ["HTML", "CSS", "JS", "React", "Node", "MongoDB"],
    img: "wasteNot.png",
    link: "https://gitlab.com/mljustinli/waste-not",
    github: "https://gitlab.com/mljustinli/waste-not",
};

var swipeNeuralNet = {
    startDate: new Date(2019, 4),
    title: "Swipe Brick Breaker <br>Neural Net",
    description: `
    Trained neural nets to learn to play Swipe Brick Breaker using
    NEAT (NeuralEvolution of Augmenting Topologies). The top score is 111.
    `,
    tags: ["HTML", "CSS", "JS", "Processing"],
    img: "swipeNeuralNet.png",
    link: "https://mljustinli.gitlab.io/swipe-block-neural-net/",
    github: "https://gitlab.com/mljustinli/swipe-block-neural-net",
};

var thisSite = {
    startDate: new Date(2018, 9),
    title: "This Website",
    description: `
    Designed a website to showcase my journey as a game developer, computer scientist,
    and web developer.
    `,
    tags: ["HTML", "CSS", "JS", "JQuery"],
    img: "background1.png",
    link: "#",
    github: "https://gitlab.com/mljustinli/project-showcase",
};

var wordGenerator = {
    startDate: new Date(2018, 9),
    title: "Word Generator",
    description: `
    This website was created to help writers overcome writer's block. While simple, this website makes it easy to begin a story and come up with ideas.
    <br><br>
    Some advantages to this website:
    <ul>
        <li><strong>Break writing up into sections.</strong> Come up with a story sentence by sentence as you're inspired by each new word.</li>
        <li><strong>Avoid procrastination.</strong> It's easy to write one sentence!</li>
        <li><strong>Believe in yourself.</strong> It may seem weird to write a story that fits random words, but you will find that it's easier than it seems. You are creative.</li>
    </ul>
    `,
    tags: ["HTML", "CSS", "JS", "JQuery"],
    img: "wordGenerator.png",
    link: "https://projects-showcase.gitlab.io/word-generator/",
    github: "https://gitlab.com/projects-showcase/word-generator",
};

var writerFacts = {
    startDate: new Date(2018, 8),
    endDate: new Date(2018, 9),
    title: "Writer Facts",
    description: `
    A precursor to the Word Generator website meant to help writers overcome
    writer's block. This website presents random facts about famous writers
    who also faced writer's block.
    `,
    tags: ["HTML", "CSS", "JS", "JQuery"],
    img: "neilGaiman.jpg",
    link: "https://projects-showcase.gitlab.io/writer-facts/",
    github: "https://gitlab.com/projects-showcase/writer-facts",
};

var dashingDino = {
    startDate: new Date(2018, 6),
    endDate: new Date(2018, 7),
    title: "Dashing Dino",
    description: `
    Experimented with 3D design and created an endless obstacle game.
    `,
    tags: ["Unity3D", "C-Sharp", "Blender"],
    img: "dashingDino.png",
    link: "https://play.google.com/store/apps/details?id=com.JuiceTin.DinoGame",
    github: "https://gitlab.com/mljustinli/dashing-dino-code-samples",
};

var bouncingBalls = {
    startDate: new Date(2018, 7),
    title: "Pinball Ball Bouncing",
    description: `
    A demo created in Processing demonstrating circle collision and ball bounce
    collision with circles.
    `,
    tags: ["Processing"],
    img: "bouncingBalls.png",
    link: "https://www.ktbyte.com/projects/project/63981/bouncing-balls",
};

var babel = {
    startDate: new Date(2017, 7),
    endDate: new Date(2018, 1),
    title: "Tower of Babel",
    description: `
    Designed a 2D exploration game based on the short story, "Tower of Babylon," by Ted Chiang.
    `,
    tags: ["Unity3D", "C-Sharp", "Piskel"],
    img: "babel.png",
    link: "https://play.google.com/store/apps/details?id=com.JuiceTin.DinoGame",
};

var fatman = {
    startDate: new Date(2017, 7),
    title: "Fat Man",
    description: `
    Developed a 2D platformer game for Android devices about eating food.
    Users roll and bounce around a map, collecting food to gain a target amount of calories.
    `,
    tags: ["Unity3D", "C-Sharp", "Piskel"],
    img: "fatman.png",
    link: "https://play.google.com/store/apps/details?id=com.JuiceTin.FatMan&hl=en",
};

var mpc = {
    startDate: new Date(2016, 5),
    endDate: new Date(2016, 7),
    title: "Minor Planet Checker",
    description: `
    Revitalized the Minor Planet Center website and developed a user-friendly
    website to allow users to access the MPC database. Check out list mode!
    `,
    tags: ["HTML", "CSS", "JS", "PHP", "Apache"],
    img: "asteroid.jpg",
    link: "https://www.minorplanetcenter.net/whats_up/",
};

var metronome = {
    startDate: new Date(2016, 8),
    title: "MetronoMe",
    description: `
    Created a customizable metronome during LexHack 2016. My friend and I won
    2nd place!
    `,
    tags: ["Android"],
    img: "androidLogo.png",
    link: "https://devpost.com/software/subdivide",
    github: "https://github.com/choiben314/metronome",
};

var rainbowFish = {
    startDate: new Date(2015, 7),
    endDate: new Date(2015, 11),
    title: "Rainbow Fish",
    description: `
    Created a fish simulation app for Android devices.
    `,
    tags: ["Unity3D", "C-Sharp", "Piskel"],
    img: "fish.png",
    github: "https://gitlab.com/mljustinli/rainbow-fish-code-samples",
    link: "https://mljustinli.github.io/rainbow-fish/",
};

// var fatman = {
//     startDate: new Date(2017, 8),
//     endDate: new Date(2017, 8),
//     title: "Fat Man",
//     description: `
//     Developed a 2D platformer game for Android devices about eating food.
//     Users roll and bounce around a map, collecting food to gain a target amount of calories.
//     `,
//     tags: ["Unity3D", "C-Sharp", "Piskel"],
//     img: "fatman.png",
//     github: "https://google.com", <-- THAT
//     link: "https://google.com",
// };

var stops = [
    foodWasteApp,
    swipeNeuralNet,
    thisSite,
    wordGenerator,
    writerFacts,
    dashingDino,
    bouncingBalls,
    babel,
    fatman,
    mpc,
    metronome,
    rainbowFish,
];

const months = ["Jan", "Feb", "March", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var projView = $("#project-view");

var colorExceptions = {
    // C-Sharp: "c-sharp"
};

// Start function
projView.html("");
for (var i = 0; i < stops.length; i++) {
    const obj = stops[i];

    //generate a list of the project's tags in html format
    var tagList = "";
    for (var j = 0; j < obj.tags.length; j++) {
        var colorClass = "";
        if (obj.tags[j] in colorExceptions) {
            colorClass = "tag-" + colorExceptions[obj.tags[j]];
        } else {
            colorClass = "tag-" + obj.tags[j].toLowerCase();
        }

        tagList += `<div class = "tag ${colorClass}">${obj.tags[j]}</div>`;
    }

    //check if the project has an end date
    var endDateHTML = "";
    if (obj.hasOwnProperty("endDate")) {
        endDateHTML = `<div class = 'end-date'>${months[obj.endDate.getMonth()] + " " + obj.endDate.getFullYear()}</div>`;
    }

    //check if the project has a github/gitlab link
    var gitLink = "";
    if (obj.hasOwnProperty("github")) {
        gitLink = `<div class = 'link-git'><a href='${obj.github}' target = '_blank'>Git Link</a></div>`;
    }

    //create project html
    var markup = `
    <div class = 'project-wrapper' id = '${"station-" + i}'>
        <div class = 'project'>
            <div class = 'project-left'>
                <div class = 'image' style="background-image: url(img/proj/${obj.img})"></div>
                <div class = 'date-wrapper'>
                    <div class = 'start-date'>${months[obj.startDate.getMonth()] + " " + obj.startDate.getFullYear()}</div>
                    <div class = 'dash-date'> - </div>
                    ${endDateHTML}
                </div>
            </div>

            <div class = 'project-right'>
                <div class = 'name'><b>${obj.title}</b></div>
                <div class = 'description'>
                    ${obj.description}
                </div>
                <div class = 'link-project'><a href='${obj.link}' target = '_blank'>Project Link</a></div>
                ${gitLink}
            </div>

            <div class = 'project-bottom'>
                ${tagList}
            </div>
        </div>
    </div>
    `;

    //add project html to project wrapper view
    projView.append(markup);
}
