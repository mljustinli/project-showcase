$(window).on('load', function(){
    populateMap(stops);
    highlightCurrStop();
});

window.addEventListener("resize", function() {
    redrawLines();
});

//----------------------------------------------------------------

var isMapShowing = false;

var currStop = 0; //right now refers to index in stops
var subStops = stops;
const mapWrapper = $("#map-wrapper");
var projView = $("#project-view");

//----------------------------------------------------------------

const nodesPerRow = 5;
const nodesWidthPerc = 4;
const nodesMargin = (100 - nodesPerRow * nodesWidthPerc) / (2 * nodesPerRow);

const mapObj = $("#map");

//populates map with train stations from scratch (empty map div) because
//size of object array may vary
//subStops --> list of stations (may not be all of them)
function populateMap(subStops) {
    mapObj.html("");

    //draws dots for each stop
    var nodeRowCount = -1;
    var row = 0;
    for (var i = 0; i < subStops.length; i++) {
        nodeRowCount++;
        if (nodeRowCount == nodesPerRow) {
            row++;
            nodeRowCount = 0;
        }

        var dir = "";
        if (row % 2 == 0) {
            dir = "station-left";
        } else {
            dir = "station-right";
        }
        var stationHTML = `
            <div class = 'train-station ${dir}' title = '${subStops[i].title}'>
            </div>
        `;

        //<div id = 'station-name'>
        //hey hey hey
        //</div>
        // mapObj.append("<div class = 'train-station " + dir + "'></div>");
        mapObj.append(stationHTML);
    }

    var stations = document.getElementsByClassName("train-station");

    for (var i = 0; i < stations.length; i++) {
        $(stations[i]).click(function() {
            currStop = $(stations).index($(this));
            highlightCurrStop();
            toggleView();
            window.location.href = location.pathname + "#station-" + $(stations).index($(this));
        });
    }

    mapObj.append("<svg id = 'line-container'></svg>");
    redrawLines();
}

$('body').keyup(function(e){
   if(e.keyCode == 32){

   }
});

function redrawLines() {
    const lineContainer = $("#line-container");
    lineContainer.html("");
    var stations = document.getElementsByClassName("train-station");

    for (var i = 0; i < stations.length - 1; i++) {
        var DOM1 = stations[i];
        var DOM2 = stations[i + 1];

        const x1 = DOM1.offsetLeft + DOM1.offsetWidth/2;
        const y1 = DOM1.offsetTop + DOM1.offsetHeight/2;
        const x2 = DOM2.offsetLeft + DOM2.offsetWidth/2;
        const y2 = DOM2.offsetTop + DOM2.offsetHeight/2;

        var newLine = document.createElementNS('http://www.w3.org/2000/svg','line');
        newLine.setAttribute('x1',x1);
        newLine.setAttribute('y1',y1);
        newLine.setAttribute('x2', x2);
        newLine.setAttribute('y2', y2);
        lineContainer.append(newLine);
    }

    var height = stations[stations.length - 1].offsetTop - stations[0].offsetTop;
    height += 100;
    //$("map-wrapper").scrollHeight
    lineContainer.height(height);
}

//highlights current stop, regardless of if displaying substations
function highlightCurrStop() {
    var stations = document.getElementsByClassName("train-station");

    for (var i = 0; i < stations.length; i++) {
        if (i == currStop) {
            $(stations[i]).addClass("station-highlight");
        } else {
            $(stations[i]).removeClass("station-highlight");
        }
    }
}

// ----------------------------------------------
// Opening map

var mapButton = $("#map-button");
mapButton.click(function() {
    toggleView();
});

function openMap() {
    projView.addClass("hidden");
    mapWrapper.removeClass("hidden");
    redrawLines();
}

function toggleView() {
    if (isMapShowing) { //show Project View
        mapButton.html("All Projects");
        mapWrapper.addClass("hidden");

        projView.removeClass("hidden");
    } else { //show Map View
        mapButton.html("Back");

        mapWrapper.removeClass("hidden");
        projView.addClass("hidden");
        redrawLines();
    }
    isMapShowing = !isMapShowing;
}
